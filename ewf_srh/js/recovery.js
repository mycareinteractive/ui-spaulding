// ------------------------------------------------------------------------------
// public functions
// ------------------------------------------------------------------------------

// This is a hack for the old primary-recovery panel design.  Since we don't have MVC in the design here we simulate a very basic MVC model.
// MODEL 		- The data passed in is a JSON object that contains all data this page needs.
// VIEW 		- We Ajax load recovery.html and recovery.css into DOM.
// CONTROLLER	- We then hijack keystroks so we can handle it first.  For those keys we ignore we let them fall back to original handler.
// 
// CSS engineer can take these 3 files (html+js+css) and start tweaking stylesheets without knowing anything about js logic.
// JS engineer can test this page alone without backend and any other javascript logic.  One page at a time, no worries about messing other pages.
function recovery (wrapper, classStr, oncreate, ondestroy, horizontalMenu, backAtBottom, noAjax) {
    this.wrapper = wrapper;
    this.classStr = classStr;
    this.oncreate = oncreate;
    this.ondestroy = ondestroy;
    this.horizontalMenu = horizontalMenu;
    this.noAjax = noAjax;
    this.backAtBottom = backAtBottom;
    this.data = null;
    this.subdata = null;
    if(this.backAtBottom)
    	this.idx = 0;
    else
    	this.idx = 1;
}

recovery.prototype.render = function(data) {
	this.data = data;
	
	if(!this.noAjax) { // Ajax loading
    	// load recovery.css
		$("<link/>", {rel: "stylesheet", type: "text/css", href: "css/recovery.css"}).appendTo("head");
		
		// load recovery.html
		var container = $('#' + this.wrapper);

		if(container.length<=0) {
			$('body').prepend('<div id="' + this.wrapper + '"></div>');
		}
		
		var context = this;
		
		$('#' + this.wrapper).load("recovery.html #recovery", function(){            // callback to outside so they can do stuff (such as hiding div)            if(context.oncreate)                context.oncreate();			applySettings("#recovery");
			context._renderData();
		});
    }
    else {
    	this._renderData();
    }
}

recovery.prototype.destroy = function() {
	// restore key handler
	this._restoreKeypressed();
	
	//remove HTML from DOM and remove CSS file link	
	$('#' + this.wrapper).remove();
	$('link[href="css/recovery.css"]').remove();
	
	// callback to outside so they can bring up other UI
	if(this.ondestroy)
		this.ondestroy();
};

// ------------------------------------------------------------------------------
// internal functions
// ------------------------------------------------------------------------------
// All the keyboard event that we will process
recovery.prototype._navigate = function(key)	{

	var curr = $("#recovery #selections a.active");

	if (curr.length>=0 && (key=='ENTER' || key=="LEFT")) {	
	
		this._click(curr,key);
		return true;
	}
	
	if ( key=='MENU' || key =='HOME') {		this.destroy();		return false;	}	if ( key=='POWR' ||	key=='END')	{		this.destroy();		return false;	}
	var menuprev = "UP", menunext = "DOWN";
	var submenuprev = "LEFT", submenunext = "RIGHT";
	if(this.horizontalMenu) {	// horizontal menu uses left/right to change focus and up/down to further control sub menu
		menuprev = "LEFT";                              
		menunext = "RIGHT";
		submenuprev = "UP";
		submenunext = "DOWN";
	}
	
	if(key==menuprev || key==menunext) {
		this._changeDataFocus(key);	
		return true;
	}
	else if(key==submenuprev || key==submenunext) {
		this._changeSubDataFocus(key);	
		return true;
	}
	msg('recovery.js - returning key to main' + key);
	return false;
};

// When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
recovery.prototype._click = function(jqobj,key) {
	
	
	var backidx = this.backAtBottom? this.subdata.length : 0;
	
	if(this.idx == backidx || key=='LEFT') { // back button
		this.destroy();
	} else {
		var dataidx = this.backAtBottom? this.idx : this.idx-1;
	if(typeof this.subdata[dataidx] != 'undefined') {

	var sub = this.subdata[dataidx];
	} else {
	var sub = this.subdata;
	}

	var newpage = new watchvideo('watchvideo-wrapper', this.idx
							, function(){
								$('#recovery-wrapper').hide();
							}, function(){
								$('#recovery-wrapper').show();
							},0,1);
							
						newpage.render(sub);
	}

	return;
};

// When an object is focused by using keyboard, or mouse hover
recovery.prototype._focus = function(jqobj) {
	jqobj.addClass('active');
	
	this.idx = jqobj.index();
	var backidx = this.backAtBottom? this.subdata.length : 0;
	if(this.idx < 0 || this.idx > this.subdata.length) {
		return;
	}
	else if(this.idx == backidx) { // back button
		$('#recovery #background').removeClass('textoverlay');
	}
	else {
		$('#recovery #background').addClass('textoverlay');
		this._renderSubData(jqobj);
	}
}

// When an object lose focus or mouse leave
recovery.prototype._blur = function(jqobj) {
	if(jqobj) {
		jqobj.removeClass('active');
	}
	else {
		$('div#recovery div#selections a.active').removeClass('active');
	}
	this.idx = -1;
	$('#recovery #selection #body').html('');
}

recovery.prototype._renderData = function() {
    $('#' + this.wrapper + " #recovery").addClass(this.classStr);
    var data = this.data;
    
	// show the div
	$('#' + this.wrapper + " #recovery").show();

	$("#recovery div#banner").html('<p class="banner">He inspired us to develop the technology that helped his remarkable recovery.</p>');			    
	$('<span></span>').appendTo('div#recovery div#banner p').html('<br/> Find your strength.');			    	
	$('<p></p>').prependTo('div#recovery div#label').html('Recovery Stories');

	$('div#recovery img#labelicon').attr('src', './images/icon_home_about.png');
	var selections = $('div#recovery div#selections');
			this.subdata = data.recovery;
			var sub = this.data.recovery;			

			var context = this;
			var firstobj = null;
	 
	// dynamically render data if neccesary                                    x
	for(var key in data.recovery) {

			var Title = data.recovery[key].TitleBrief;
			var localEntryUID = data.recovery[key].localEntryUID;
			var o = $('<a href="#"></a>').appendTo(selections).html(Title).addClass('menu ' + key);
		
	}
		
			if(this.backAtBottom) {
				$('<a class="back" href="#" title="back">&lt; BACK</a>').appendTo(selections);
			}
			else {
				var firstobj = $('<a class="back" href="#" title="back">&lt; BACK</a>').prependTo(selections);
				
			}			
			this._focus(firstobj);
			
			// wire up the mouse event
			$('div#recovery div#selections a').click(function(){
				context._blur();
				context._focus($(this));
				context._click($(this));
				return false;
			});
			
	// now we take over the key handler from navigation.js
	this._overwriteKeypressed();
};

recovery.prototype._renderSubData = function(jqobj) {
	var dataidx = this.backAtBottom? this.idx : this.idx-1;
	if(typeof this.subdata[dataidx] != 'undefined') {
	var sub = this.subdata[dataidx];	
	} else {
	var sub = this.subdata;
	}
	this._renderPage(sub);	
}

recovery.prototype._renderPage = function(jqobj) {
	var directions = 'We invite you to watch these compelling and unforgettable stories, and learn how <span>Spaulding Rehabilitation Network</span> changes people&#39s lives. <br/><br/><span>Press Select to watch a video.</span>'
		
	$("#recovery #selection #body").html('<p class="submenu active">' + directions + '</p>');
}

// Hijack the global keypressed() function
recovery.prototype._overwriteKeypressed = function() {
	this.original_keypressed = window.keypressed;
	var context = this;
	window.keypressed = function(keyCode) {
		context._keypressed(keyCode);
	}
};

// Restore the key handler
recovery.prototype._restoreKeypressed = function() {
	window.keypressed = this.original_keypressed;
};

recovery.prototype._keypressed = function(keyCode) {
	var akey = getkeys(keyCode);
	
	if(window['msg'])	
		msg('(recovery.js?) keypress: code = ' + keyCode + ' - ' + akey  );
	
	// remember to let it fall back to original handler if we ignore it
	if(!this._navigate(akey)) {
		if(this.original_keypressed)
			return this.original_keypressed(keyCode);
	}
	
	return true;
};

// Focus control
recovery.prototype._changeDataFocus = function(key) {

	var curr = $('div#recovery div#selections a.active');
	var next = curr;
msg(curr);	
	if(curr.length <= 0) {
		this.idx = -1;
		return;
	}
		
	switch(key) {
		case 'UP':
		case 'LEFT':
			next = curr.prev();
			if(next.length<=0)
				next = $('div#recovery div#selections a:last-child');
			break;
		case 'DOWN':
		case 'RIGHT':
			next = curr.next();
			if(next.length<=0)
				next = $('div#recovery div#selections a:first-child');
			break;
	}
	
	this._blur(curr);
	this._focus(next);
};

// Focus control
recovery.prototype._changeSubDataFocus = function(key) {
	
	var curr = $('div#recovery div#selection #body .submenu.active');
	var next = curr;

	if(curr.length <= 0) {
		return;
	}
		
	switch(key) {
		case 'UP':
		case 'LEFT':
			next = curr.prev();
			if(next.length<=0)
				next = $('div#recovery div#selection #body .submenu:last-child');
			break;
		case 'DOWN':
		case 'RIGHT':
			next = curr.next();
			if(next.length<=0)
				next = $('div#recovery div#selection #body .submenu:first-child');
			break;
		case 'ENTER':
			
					var dataidx = this.backAtBottom? this.idx : this.idx-1;
					if(typeof this.subdata[dataidx] != 'undefined') {
						var sub = this.subdata[dataidx];	
						var newpage = new watchvideo('watchvideo-wrapper', 'recovery'
							, function(){
								$('#recovery').hide();
							}, function(){
								$('#recovery').show();
							});							
					msg(sub);
							newpage.render(sub);
						}
					
			break;

	}
	
	if(next != curr) {
		curr.removeClass('active');
		next.addClass('active');
	}
}

