function buildmyview()	{
	// hide everything
	$("#primary").hide();
	
	// load myview.css
	$("<link/>", {rel: "stylesheet", type: "text/css", href: "css/myview.css"}).appendTo("head");
	
	// load myview.html


	var myviewHTML = myview_getmyviewHTML();
	var myview = '<div id="myview">';  
	myview = myview + myviewHTML.myview;
	myview = myview + '</div>';
	myview = myview + '<div id="K_myview_active"></div>'
	$('body').prepend(myview);
	$('#myview').css("display","block");
	$('#myview').load();

	var settings = getsettings();
	if (settings)
		$("#myview").addClass(settings);
	
	if($("#K_images").text()) 
			$("#myview a.noimages").addClass('selected');
	if($("#K_audio").text()) 
			$("#myview a.audio").addClass('selected');

	var contrast = $("#K_contrast").text();
	if(contrast)
			$("#myview a." + contrast).addClass('selected');		
	
	$("#myview #options a.noimages").addClass('active');
	$("#K_myview_active").addClass("noimages");
	
	myview_overwriteKeypressed();
}

function myview_overwriteKeypressed() {
	window.navigation_keypressed = window.keypressed;
	window.keypressed = myview_keypressed;
}

function myview_destroy() {

	// restore key handler
	myview_restoreKeypressed();	
	$('#myview').hide();
	//remove HTML from DOM and remove CSS file link	
	$('#myview').remove();
	$('link[href="css/myview.css"]').remove();
	$('#primary').css("display","block");
};


function myview_restoreKeypressed() {
	window.keypressed = navigation_keypressed;
}

function myview_keypressed(keyCode) {
	var akey = getkeys(keyCode);
	
	msg('(myview.js) keypress: code = ' + keyCode + ' ' + akey  );
	
	myview_navmyview(akey);
}

function myview_navmyview(key)	{
	
	var previous = $("#K_myview_active").attr("class");

	
	if (previous&&key=='ENTER')	{
		myview_process(previous);
		return;
	}

	if ( key=='MENU' || key =='HOME') {
		this.destroy();
		return false;
	}

	if ( key=='POWR' ||	key=='END')	{
		this.destroy();
		return false;
	}

	var active = myview_getActive(key);
	msg('myview.js active ' + active + ' previous ' + previous);
	
	$("#K_myview_active").removeClass(previous);
	$("#myview a."+previous).removeClass('active');
	$("#myview a."+active).addClass('active');
	$("#K_myview_active").addClass(active);
	
	return;

}

function myview_process(active)	{
		
	msg('(myview.js) myview_process ' + active);
	var panel = $("#K_panel").attr("class");

	if (active=='cancel') {
		myview_destroy();
	
		if(panel=='primary') {			
			gotomainmenu();
		} else
			buildintro();
		
	
	} else
		if (active=='save')	{
			var images		= '';
			var contrast	= '';
			var audio		= '';
			if ($("#myview a.noimages").hasClass('selected'))
				images 		= 'noimages';
			if ($("#myview a.lightdark").hasClass('selected'))
				contrast 	= 'lightdark';			
			if ($("#myview a.darklight").hasClass('selected'))
				contrast 	= 'darklight';			
			if ($("#myview a.hearaudio").hasClass('selected'))
				audio		= 'hearaudio';
			updatemyview(images,contrast,audio);
			submitConfigs(images,contrast,audio);

			myview_destroy();			

		if(panel=='primary') {
			gotomainmenu();
		} else
			buildintro();
	
		
	}
	
	if ($("#myview a."+active).hasClass('selected'))	{
		$("#myview a."+active).removeClass('selected');
		$("#myview").removeClass(active);
	}	else	{
			$("#myview a."+active).addClass('selected');
			$("#myview").addClass(active);
		}
			
	if ((active=='lightdark')&&($("#myview a.darklight").hasClass('selected')))	{
		$("#myview a.darklight").removeClass('selected');
		$("#myview").removeClass('darklight');
	}	else
		if ((active=='darklight')&&($("#myview a.lightdark").hasClass('selected')))	{
			$("#myview a.lightdark").removeClass('selected');
			$("#myview").removeClass('lightdark');
		}
	
	
	return;

}


function updatemyview(images,contrast,audio)	{
	
	$("#K_audio").text(audio);
	$("#K_contrast").text(contrast);
	$("#K_images").text(images);


	
	return;
}


function myview_getActive(key)	{

	var navigation  = "noimages,lightdark,darklight,audio,save,cancel";
	var nav 		= navigation.split(',');
	var l			= nav.length;
	var previous 	=  $("#K_myview_active").attr("class");
	
	
	var p=0;
	var a=0;
	l--;
			
	for(i=0;i<=l;i++)	{
		if (previous==nav[i])
			p=i;
	}
	
	a=p;
	
	if (key=='UP')	{
		a--;
		if (a<0)	
			a=l;
	}	else
		if (key=='DOWN')	{
			a++;
			if (a>l)	
				a=0;
		}	else
			if (key=='LEFT')	{
				a--;
				if (a<0)	
					a=l;
			}	else
				if (key=='RIGHT')	{
					a++;
					if (a>l)	
						a=0;
				}
				

	return nav[a];
}


function myview_getmyviewHTML()	{
	
	var text 	= myview_getText();
	var panel	= '';

	panel = panel+'<div id="heading"><p>'+text.myview+'</p></div><!-- #heading -->';
	panel = panel+'<div id="instruction"><p>'+text.instruction3+'</p></div><!-- #instruction -->';
	panel = panel+'<div id="options">';
	panel = panel+'	<a class="noimages active">'+text.noimages+'</a>';
	panel = panel+'	<p>'+text.instruction4+'</p>';
	panel = panel+'	<a class="lightdark">'+text.lightdark+'</a>';
	panel = panel+'	<a class="darklight">'+text.darklight+'</a>';
	panel = panel+'	<p>'+text.instruction5+'</p>';
	panel = panel+'	<a class="audio">'+text.audio+'</a>';
	panel = panel+'</div><!-- #options -->';
	panel = panel+'<div id="buttons">';
	panel = panel+'	<a class="save">'+text.save+'</a>';	
	panel = panel+'	<a class="cancel">'+text.cancel+'</a>';
	panel = panel+'</div><!-- #buttons -->';	

	
	var myviewHTML = Array();

	myviewHTML['myview'] = panel;
	myviewHTML['active'] = 'noimages';

	return myviewHTML;	
}

function myview_getText()	{
	
	var text =	{
		
		"myview":			"My View - Profile",
		"iconpath":			"../images/",
		"instruction3":		"<span>We display images and color on the screen.</span> If you would like special Video or Audio features, use your Up/Down Arrow keys to scroll, then SELECT to check special preferences below.",
		"instruction4":		"Select a High Contrast Profile:",
		"instruction5":		"Select Audio Profile (default is No Audio):",
		"noimages":			"No Images Visible",
		"lightdark":		"High Contrast: Light Text on Dark",
		"darklight":		"High Contrast: Dark Text on Light",
		"audio":			"Hear Audios on Main Menus (Home screen)",
		"save":				"SAVE",
		"cancel":			"CANCEL - GO BACK",
	}
	
	return text;
}


