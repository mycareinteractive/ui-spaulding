/********************************************************************************
 * @brief																		*
 * 		Play TV Functions					               						*
 *																				*
 * @author																		*
 *		Bill Sears\n															*
 *		Aceso\n																	*
 *		http://www.aceso.com\n													*
 *																				*
 * @modified																	*
 * 		Tami Seago, 02/05/2013, add info and error handling						*
 ********************************************************************************/

function playTV(channel)	{
msg('channel  ' + channel);	
	var grid = $("#K_grid").attr("class");
	var ewf = ewfObject();
var panel   = $("#K_panel").attr("class");	

	var url = '';
	var channels 	  = epgChannels();
	var programNumber = '';
	var frequency 	  = '';
	var channelNumber = '';
	var channelName   = '';
	var scenictvF     = '57000';
	var channelUsage  = '';
	var version = $("#K_version").text();
msg(version);	
	
	$.each(channels['Channels'], function(i,row){	
		if (channel==row["channelID"])	{
			$("#VIDEO_nowplaying").text(channel);
			programNumber = row["programNumber"];
			frequency	  = row["frequency"];
			channelNumber = row["channelNumber"];
			channelName   = row["channelName"];

			if (row["channelUsage"])
				channelUsage = ' ChannelUsage="'+row["channelUsage"]+'"';
	
			var overlay = '<p>'+channelNumber+' '+channelName+'</p>';
			$("#overlay #channelinfo").html(overlay);
			$("#overlay").show();
			$("#overlay #channelinfo").show();
			return false;
		}		
	});	
	if (version == 'ENSEO') {
	var parm1 = 'PhysicalChannelIDType=Freq';
	var parm2 = 'PhysicalChannelID="'+frequency+'"';
	var parm3 = 'DemodMode="QAM'+ewf.EPGChannelQAMMode+'"';
	var parm4 = 'ProgramSelectionMode="'+ewf.EPGProgramSelectionMode+'"';
	var parm5 = 'ProgramID="'+programNumber+'"';
	
	var isDigital = true;
	var prgNum = parseInt(programNumber);
	
	if (isNaN(prgNum) || prgNum <= 0) {
	    isDigital = false;
	}
	
	if(isDigital) {
		var parms = ' ' + parm1 + ' ' + parm2 + ' ' + parm3 + ' ' + parm4 + ' ' + parm5 + ' ';
		url = '<ChannelParams ChannelType="Digital"'+channelUsage+'><DigitalChannelParams'+parms+'></DigitalChannelParams></ChannelParams>';
	}
	else {
		var parms = ' '+parm1+' '+parm2+' ';
		url = '<ChannelParams ChannelType="Analog"'+channelUsage+'><AnalogChannelParams'+parms+'></AnalogChannelParams></ChannelParams>';
	}
	var sessionID = getSessionID();
		var player = Nimbus.getPlayer();
	if (player) {
		player.stop();
		player.close();
	}
		var player = Nimbus.getPlayer( url, null, sessionID );
		if (player)	{
	
			player.setChromaKeyColor(0x00000000);
			player.setVideoLayerBlendingMode("colorkey");
			player.setVideoLayerTransparency(1);
			player.setPictureFormat("Widescreen");
			player.setVideoLayerRect(0, 0, 1280, 720);
			player.setVideoLayerEnable(true);
			player.play();
		}
	}

	$("#overlay #channelinfo").delay(3000).hide(50);
	if (panel!='discharged')
		$('#overlay').delay(3000).hide(50);
	return url;
}



function getSessionID()	{

	var mac	= getDevice();
	var sessionID = 'SEC'+mac.substr(7,5);

	var d = new Date();
	var day		= d.getDate();
	var month	= d.getMonth() + 1;	
	var hours 	= d.getHours();
	var minutes	= d.getMinutes();
	var seconds	= d.getSeconds();

	var year 	= d.getFullYear() - 2000;
	
	day 	= fixZero(day);
	month 	= fixZero(month);
	hours 	= fixZero(hours);
	minutes = fixZero(minutes);
	seconds = fixZero(seconds);
	
	var sessionDate = day+month+year+hours+minutes+seconds;
	
	sessionID = sessionID+sessionDate;
	
	return sessionID;	
}

function donothing()	{
	return;
}

function fixZero(val)	{
	val = ''+val;
	if (val.length==1) 
		val = '0'+val;
	return val;
}

function scenicTV()	{
	var ewf = ewfObject();
	$("#K_panel").removeAttr('class');
	$("#K_panel").addClass('scenictv');
	$("#watchtv").hide();

	msg('scenictvChannel ' +ewf.scenictvChannel);
	var url = playTV('C'+ewf.scenictvChannel);
	
	return url;
}


function menuTV()	{
	var ewf = ewfObject();
	$("#K_panel").removeAttr('class');
	$("#K_panel").addClass('menutv');
	$("#watchtv").hide();

	msg('menuChannel ' +ewf.menuChannel);
	var url = playTV('C'+ewf.menuChannel);
	
	return url;
}


function introTV()	{
	var ewf = ewfObject();
	$("#K_panel").removeAttr('class');
	$("#K_panel").addClass('introtv');
	$("#watchtv").hide();

	msg('menuChannel ' +ewf.startChannel);
	var url = playTV('C'+ewf.startChannel);
	
	return url;
}




function updnChannel(key)	{

	var channels = epgChannels();
	var channel  = $("#VIDEO_nowplaying").text();
	var curr     = '';
	var prev     = '';
	var next     = '';
	var first	 = '';
	var last	 = '';
	
	$.each(channels['Channels'], function(i,row){
		if (first=='') 
			first = row["channelID"];
		if (curr!=''&&next=='')   	
			next = row["channelID"];
		last = row["channelID"];
		if (channel==row["channelID"])	{
			curr = row["channelID"];
		}
		if (curr=='')
			prev = row["channelID"];
	});
	if (prev=='') prev = last;
	if (next=='') next = first;
	
	if (key=='CHUP') channel = next;
		else
		if (key=='CHDN') channel = prev;
		
	var stoptv = stopTV();
	var playtv = playTV(channel);	
	
	return playtv;
}


function keyChannel(key)	{

	var cLength = 1;
	var keycheck = $("#K_keycheck").text();
	if (keycheck)	{
		clearTimeout(keycheck);
		$("#K_keycheck").text('');
	}
	
	var pKey = $("#K_key").text();
	
	if (pKey)	{
		$("#K_key").text('');
		pKey = pKey+key;
		cLength = pKey.length;
		if 	(cLength == 3)	{
			key3Channel(pKey);
			return;
		}
		key = pKey;
	}

	if (cLength==1&&key=='0')	
		return;
	
	$("#K_key").text(key);
	var keycheck = setTimeout( "key2Channel()", 2000 );
	$("#K_keycheck").text(keycheck);

	var overlay = '<p>'+key+'_</p>';
	$("#overlay #channelinfo").html(overlay);
	$("#overlay #channelinfo").show();
	$("#overlay").show();	
	return;

}


function key2Channel()	{
	
	var keycheck = $("#K_keycheck").text();
	clearInterval(keycheck);
	$("#K_keycheck").text('');

	var pKey = $("#K_key").text();	
	$("#K_key").text('');
	
	key3Channel(pKey);	
}

function key3Channel(pKey)	{
	
	//msg('key3Channel');

	var channels 	  = epgChannels();
	var channel		  = '';
	
	$.each(channels['Channels'], function(i,row){	
										  
		if (pKey==row["channelNumber"])	{
			channel = row["channelID"];
			//msg('channel: '+pKey+' '+channel);
			return false;
		}
		
	});	
	
	if (channel)	{
		stopTV();
		playTV(channel);
	}	else	{
			var overlay = '<p>'+pKey+' not available</p>';
			$("#overlay #channelinfo").html(overlay);
			$("#overlay").show(50).delay(3000).hide(50);
			$("#overlay #channelinfo").show(50).delay(3000).hide(50);	
		}
	
	return;
}

function stopTV()	{

	$("#overlay #channelinfo").hide();
	$("#overlay").hide();

	var version = $("#K_version").text();
	msg('stop tv' +version);
	if (version=='ENSEO')	{
		var player = Nimbus.getPlayer();
		if (player)	{
			player.setVideoLayerEnable(false);
			player.stop();
			player.close();
		}	
	}

	return;	
}



function setCC()	{
msg('in setCC');
	var version = $("#K_version").text();
	if (version=='ENSEO')	{
		var cc = Nimbus.getCCMode();
		if (cc=='Off')	{
			Nimbus.setCCMode('On');
			msg('Closed Captioning has been turned ON');

		}	else

			if (cc=='On')	{

				Nimbus.setCCMode('Off');

				msg('Closed Captioning has been turned OFF');

			}

	}

	

	return;	

}



