// ------------------------------------------------------------------------------
// public functions
// ------------------------------------------------------------------------------

// This is a hack for the old primary-secondary panel design.  Since we don't have MVC in the design here we simulate a very basic MVC model.
// MODEL 		- The data passed in is a JSON object that contains all data this page needs.
// VIEW 		- We Ajax load tertiary.html and tertiary.css into DOM.
// CONTROLLER	- We then hijack keystroks so we can handle it first.  For those keys we ignore we let them fall back to original handler.
// 
// CSS engineer can take these 3 files (html+js+css) and start tweaking stylesheets without knowing anything about js logic.
// JS engineer can test this page alone without backend and any other javascript logic.  One page at a time, no worries about messing other pages.
function tertiary (wrapper, classStr, oncreate, ondestroy, horizontalMenu, backAtBottom, noAjax) {
    this.wrapper = wrapper;
    this.classStr = classStr;
    this.oncreate = oncreate;
    this.ondestroy = ondestroy;
    this.horizontalMenu = horizontalMenu;
    this.noAjax = noAjax;
    this.backAtBottom = backAtBottom;
    this.data = null;
    this.subdata = null;
    
    if(this.backAtBottom)
    	this.idx = 0;
    else
    	this.idx = 1;
}

tertiary.prototype.render = function(data) {
	this.data = data;
	
	if(!this.noAjax) { // Ajax loading
    	// load tertiary.css
		$("<link/>", {rel: "stylesheet", type: "text/css", href: "css/tertiary.css"}).appendTo("head");
		
		// load tertiary.html
		var container = $('#' + this.wrapper);
		if(container.length<=0) {
			$('body').prepend('<div id="' + this.wrapper + '"></div>');
		}
		
		var context = this;
		
		$('#' + this.wrapper).load("tertiary.html #tertiary", function(){            // callback to outside so they can do stuff (such as hiding div)            if(context.oncreate)                context.oncreate();            applySettings('#tertiary');    
			context._renderData();
		});
    }
    else {
    	this._renderData();
    }
}

tertiary.prototype.destroy = function() {
	// restore key handler
	this._restoreKeypressed();
	
	//remove HTML from DOM and remove CSS file link	
	$('#' + this.wrapper).remove();
	$('link[href="css/tertiary.css"]').remove();
	
	// callback to outside so they can bring up other UI
	if(this.ondestroy)
		this.ondestroy();
};

// ------------------------------------------------------------------------------
// internal functions
// ------------------------------------------------------------------------------
// All the keyboard event that we will process
tertiary.prototype._navigate = function(key)	{
	
	var curr = $("#tertiary #selections a.active");
	
	if (curr.length>=0 && (key=='ENTER' || key=="LEFT")) {	
		this._click(curr,key);
		return true;
	}
	if ( key=='MENU' || key =='HOME') {		this.destroy();		return false;	}	if ( key=='POWR' ||	key=='END')	{		this.destroy();		return false;	}
	var menuprev = "UP", menunext = "DOWN";
	var submenuprev = "LEFT", submenunext = "RIGHT";
	if(this.horizontalMenu) {	// horizontal menu uses left/right to change focus and up/down to further control sub menu
		menuprev = "LEFT";
		menunext = "RIGHT";
		submenuprev = "UP";
		submenunext = "DOWN";
	}
	
	if(key==menuprev || key==menunext) {
		this._changeDataFocus(key);	
		return true;
	}
	else if(key==submenuprev || key==submenunext) {
		this._changeSubDataFocus(key);	
		return true;
	}
	
	return false;
};

// When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
tertiary.prototype._click = function(jqobj,key) {
	var backidx = this.backAtBottom? this.subdata.length : 0;	if(this.idx == backidx || key=='LEFT') { // back button		this.destroy();	} else {		var dataidx = this.backAtBottom? this.idx : this.idx-1;    	if(typeof this.subdata[dataidx] != 'undefined') {			var sub = this.subdata[dataidx];    	} else {			var sub = this.subdata;    	}    	var type = sub['type'];		var subclass = sub['tag'];    	   if(sub['type']=='menu') {			    			// this is a 4thlevel menu.    			// here we use the new MVC page - quaternary object    			var newpage = new quaternary('quaternary-wrapper', subclass    				, function(){    					$('#tertiary-wrapper').hide();    				}, function(){    					$('#tertiary-wrapper').show();    				});    				    			newpage.render(sub);   	   }			}	return;};

// When an object is focused by using keyboard, or mouse hover
tertiary.prototype._focus = function(jqobj) {
	jqobj.addClass('active');
	this.idx = jqobj.index();
	var backidx = this.backAtBottom? this.subdata.length : 0;
	if(this.idx < 0 || this.idx > this.subdata.length) {
		return;
	}
	else if(this.idx == backidx) { // back button
		$('#tertiary #background').removeClass('textoverlay');
	}
	else {
		$('#tertiary #background').addClass('textoverlay');
		this._renderSubData(jqobj);
	}
}

// When an object lose focus or mouse leave
tertiary.prototype._blur = function(jqobj) {
	if(jqobj) {
		jqobj.removeClass('active');
	}
	else {
		$('div#tertiary div#selections a.active').removeClass('active');
	}
	this.idx = -1;
	$('#tertiary #selection #body').html('');
}

tertiary.prototype._renderData = function() {
    $('#' + this.wrapper + " #tertiary").addClass(this.classStr);
    var data = this.data;
    	// show the div
	$('#' + this.wrapper + " #tertiary").show();		//$("#tertiary div#banner").html('<p class="banner">He inspired us to develop the technology that helped his remarkable recovery.</p>');			    	//$('<span></span>').appendTo('div#tertiary div#banner p').html('<br/> Find your strength.');			    	
	$('div#tertiary img#labelicon').attr('src', './images/icon_home_about.png');	
	// dynamically render data if neccesary
	for(var key in data) {
		if (key=='tag') {	// tag
		}
		else if(key=='label') {	// label
			$('<p></p>').prependTo('div#tertiary div#label').html(data[key]);
		}
		else if(key=='attributes') { // attributes (title and label icon)
			$.each(data.attributes.attribute, function(i, v) {
			    if (v.class == 'banner1') {
			        $('<p></p').appendTo('div#tertiary div#banner').html(v.text + '<br>');
			        return;
			    }
			    else if (v.class == 'banner2') {
			    	$('<span></span>').appendTo('div#tertiary div#banner p').html(v.text);
			    	return;
			    }
			    else if (v.class == 'image') {
			        $('div#tertiary img#labelicon').attr('src', './images/'+v.image);
			        return;
			    }
			});
		}
		else if(key==data.tag) { // sub menu or children
			this.subdata = data[key];
			var context = this;
			var firstobj = null;
			var selections = $('div#tertiary div#selections');
			$.each(data[key], function(i, v) {
				var o = $('<a href="#"></a>').appendTo(selections).html(v.label).addClass('menu ' + v.tag);
				if(i==0)
					firstobj = o;
			});
			
			if(this.backAtBottom) {
				$('<a class="back" href="#" title="back">&lt; BACK</a>').appendTo(selections);
			}
			else {
			var firstobj =	$('<a class="back" href="#" title="back">&lt; BACK</a>').prependTo(selections);
			}
			this._focus(firstobj);
			
			// wire up the mouse event
			$('div#tertiary div#selections a').click(function(){
				context._blur();
				context._focus($(this));
				context._click($(this));
				return false;
			});
		}
	}
	
	// now we take over the key handler from navigation.js
	this._overwriteKeypressed();
};

tertiary.prototype._renderSubData = function(jqobj) {
	var dataidx = this.backAtBottom? this.idx : this.idx-1;
	var sub = this.subdata[dataidx];
	if(sub['type']=='page' || sub['type']=='menu') {
		this._renderPage(sub);
	}
	else if(sub['type']=='pagelist') {
		this._renderPageList(sub);
	}
}

tertiary.prototype._renderPage = function(jqobj) {
	var sub = jqobj;
	if(sub && sub['attributes'] && sub.attributes.attribute) {
		var attrlist = sub.attributes.attribute;
		var len = attrlist.length;
		for(var i=0; i<len; i++) {
			var attr = attrlist[i];
			if(attr.class=="text4") { // text in page
				$("#tertiary #selection #body").html('<p class="submenu active">' + attr.text + '</p>');
			}
		}
	}
}

tertiary.prototype._renderPageList = function(jqobj) {
	var sub = jqobj;
	if(sub && sub['attributes'] && sub.attributes.attribute) {
		var attrlist = sub.attributes.attribute;
		var len = attrlist.length;
		var isPage1 = true;
		var pagebody = $('#tertiary #selection #body').html('');
		var pages = $('<div id="pages"></div>').appendTo(pagebody);
		for(var i=0; i<len; i++) {
			var attr = attrlist[i];
			if(attr.class=="text4") { // text in pagelist
				var attrstr;
				if(isPage1) {
					isPage1 = false;
					attrstr = ' class="submenu active"';
				}
				else {
					attrstr=' class="submenu"';	
				}
				pages.append('<p id=p' + i + attrstr + '>' + attr.text + '</p>');
			}
		}
		
		// support mouse driven page up/down
		var context = this;
		$('<div id="nextpage"></div>').appendTo(pagebody).click(function(){
			context._changeSubDataFocus('RIGHT');
			return false;
		});
		$('<div id="prevpage"></div>').appendTo(pagebody).click(function(){
			context._changeSubDataFocus('LEFT');
			return false;
		});
	}
}

// Hijack the global keypressed() function
tertiary.prototype._overwriteKeypressed = function() {
	this.original_keypressed = window.keypressed;
	var context = this;
	window.keypressed = function(keyCode) {
		context._keypressed(keyCode);
	}
};

// Restore the key handler
tertiary.prototype._restoreKeypressed = function() {
	window.keypressed = this.original_keypressed;
};

tertiary.prototype._keypressed = function(keyCode) {
	var akey = getkeys(keyCode);
	
	if(window['msg'])	
		msg('(tertiary.js) keypress: code = ' + keyCode + ' - ' + akey  );
	
	// remember to let it fall back to original handler if we ignore it
	if(!this._navigate(akey)) {
		if(this.original_keypressed)
			return this.original_keypressed(keyCode);
	}
	
	return true;
};

// Focus control
tertiary.prototype._changeDataFocus = function(key) {

	var curr = $('div#tertiary div#selections a.active');
	var next = curr;
	
	if(curr.length <= 0) {
		this.idx = -1;
		return;
	}
		
	switch(key) {
		case 'UP':
		case 'LEFT':
			next = curr.prev();
			if(next.length<=0)
				next = $('div#tertiary div#selections a:last-child');
			break;
		case 'DOWN':
		case 'RIGHT':
			next = curr.next();
			if(next.length<=0)
				next = $('div#tertiary div#selections a:first-child');
			break;
	}
	
	this._blur(curr);
	this._focus(next);
};

// Focus control
tertiary.prototype._changeSubDataFocus = function(key) {
	
	var curr = $('div#tertiary div#selection #body .submenu.active');
	var next = curr;
	
	if(curr.length <= 0) {
		return;
	}
		
	switch(key) {
		case 'UP':
		case 'LEFT':
			next = curr.prev();
			if(next.length<=0)
				next = $('div#tertiary div#selection #body .submenu:last-child');
			break;
		case 'DOWN':
		case 'RIGHT':
			next = curr.next();
			if(next.length<=0)
				next = $('div#tertiary div#selection #body .submenu:first-child');
			break;
	}
	
	if(next != curr) {
		curr.removeClass('active');
		next.addClass('active');
	}
}

