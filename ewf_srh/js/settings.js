// ------------------------------------------------------------------------------
// public functions
// ------------------------------------------------------------------------------

// This is a hack for the old primary-settings panel design.  Since we don't have MVC in the design here we simulate a very basic MVC model.
// MODEL        - The data passed in is a JSON object that contains all data this page needs.
// VIEW         - We Ajax load Settings.html and Settings.css into DOM.
// CONTROLLER   - We then hijack keystroks so we can handle it first.  For those keys we ignore we let them fall back to original handler.
// 
// CSS engineer can take these 3 files (html+js+css) and start tweaking stylesheets without knowing anything about js logic.
// JS engineer can test this page alone without backend and any other javascript logic.  One page at a time, no worries about messing other pages.
function Settings (wrapper, classStr, oncreate, ondestroy, horizontalMenu, backAtBottom, noAjax) {
    this.wrapper = wrapper;
    this.classStr = classStr;
    this.oncreate = oncreate;
    this.ondestroy = ondestroy;
    this.horizontalMenu = horizontalMenu;
    this.noAjax = noAjax;
    this.backAtBottom = backAtBottom;
    this.data = null;
    this.subdata = null;
    if(this.backAtBottom)
        this.idx = 0;
    else
        this.idx = 1;
}

Settings.prototype.render = function(data) {
    this.data = data;
    
    if(!this.noAjax) { // Ajax loading
        // load Settings.css
        $("<link/>", {rel: "stylesheet", type: "text/css", href: "css/settings.css"}).appendTo("head");
        
        // load Settings.html
        var container = $('#' + this.wrapper);

        if(container.length<=0) {
            $('body').prepend('<div id="' + this.wrapper + '"></div>');
        }
        
        var context = this;
        
        $('#' + this.wrapper).load("settings.html #settings", function(){
            // callback to outside so they can do stuff (such as hiding div)
            if(context.oncreate)
                context.oncreate();
                
            context._renderData();
        });
    }
    else {
        this._renderData();
    }
}

Settings.prototype.destroy = function() {
    // restore key handler
    this._restoreKeypressed();
    
    //remove HTML from DOM and remove CSS file link 
    $('#' + this.wrapper).remove();
    $('link[href="css/Settings.css"]').remove();
    
    // callback to outside so they can bring up other UI
    if(this.ondestroy)
        this.ondestroy();
};

// ------------------------------------------------------------------------------
// internal functions
// ------------------------------------------------------------------------------
// All the keyboard event that we will process
Settings.prototype._navigate = function(key)    {

    var curr = $("#settings #options a.active");

    if (curr.length>=0 && (key=='ENTER')) {  
        return this._click(curr,key);
    }
    
	if ( key=='MENU' || key =='HOME') {
		this.destroy();
		return false;
	}

	if ( key=='POWR' ||	key=='END')	{
		this.destroy();
		return false;
	}
    
    var navkeys = ["UP", "DOWN", "LEFT", "RIGHT"];
    
    if(navkeys.indexOf(key) != -1) {
        this._changeDataFocus(key); 
        return true;
    }
    
    return false;
};

// When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
Settings.prototype._click = function(jqobj,key) {
    
    var navigation  = ['noimages','lightdark','darklight','hearaudio','save','cancel'];
    var activeClass = navigation[jqobj.index('#settings #options a')];
    
    if(activeClass == 'cancel' || key=='LEFT') { // back button
        this.destroy();
        return true;
    } 
    else if(activeClass == 'save') {
        
        var noimages = false, lightdark = false, darklight = false, audio = false;
        
        if ($("#settings a.noimages").hasClass('selected'))
            noimages      = true;
            
        if ($("#settings a.lightdark").hasClass('selected'))
            lightdark    = true;          
        else if ($("#settings a.darklight").hasClass('selected'))
            darklight    = true;
                      
        if ($("#settings a.hearaudio").hasClass('selected'))
            audio       = true;
        
        cacheSettings(noimages, lightdark, darklight, audio);
        submitConfigs(noimages, lightdark, darklight, audio);
        this.destroy();
        return true;
        
    } 
    
    if (jqobj.hasClass('selected')) {
        jqobj.removeClass('selected');
        $("#settings").removeClass(activeClass);
    }   
    else {
        jqobj.addClass('selected');
        $("#settings").addClass(activeClass);
        
        // lightdark and darklight are exclusive, you can't have both
        if (activeClass=='lightdark' && $("#settings a.darklight").hasClass('selected')) {
            $("#settings a.darklight").removeClass('selected');
            $("#settings").removeClass('darklight');
        }   
        else if (activeClass=='darklight' && $("#settings a.lightdark").hasClass('selected')) {
            $("#settings a.lightdark").removeClass('selected');
            $("#settings").removeClass('lightdark');
        }
    }
    
    return true;
};


// When an object is focused by using keyboard, or mouse hover
Settings.prototype._focus = function(jqobj) {
    jqobj.addClass('active');
}

// When an object lose focus or mouse leave
Settings.prototype._blur = function(jqobj) {
    if(jqobj) {
        jqobj.removeClass('active');
    }
    else {
        $('div#settings div#options a.active').removeClass('active');
    }
    this.idx = -1;
}

Settings.prototype._renderData = function() {
    $('#' + this.wrapper + " #settings").addClass(this.classStr);
    var data = this.data;
    
    // show the div
    $('#' + this.wrapper + " #settings").show();
    var settings = getSettings();
    if (settings.length>0) {
        $("#settings").addClass(settings);
        
        if(settings.indexOf('hearaudio') != -1)
            $('div#settings div#options a.hearaudio').addClass('selected');
            
        if(settings.indexOf('lightdark') != -1)
            $('div#settings div#options a.lightdark').addClass('selected');
        else if(settings.indexOf('darklight') != -1)
            $('div#settings div#options a.darklight').addClass('selected');
            
        if(settings.indexOf('noimages') != -1)
            $('div#settings div#options a.noimages').addClass('selected');
    }
    
    var firstobj = $('div#settings div#options a:first-child');            
    this._focus(firstobj);
    var context = this;
    // wire up the mouse event
    $('div#settings div#options a').click(function(){
        context._blur();
        context._focus($(this));
        context._click($(this));
        return false;
    });
            
    // now we take over the key handler from navigation.js
    this._overwriteKeypressed();
};
// Hijack the global keypressed() function
Settings.prototype._overwriteKeypressed = function() {
    this.original_keypressed = window.keypressed;
    var context = this;
    window.keypressed = function(keyCode) {
        context._keypressed(keyCode);
    }
};

// Restore the key handler
Settings.prototype._restoreKeypressed = function() {
    window.keypressed = this.original_keypressed;
};

Settings.prototype._keypressed = function(keyCode) {
    var akey = getkeys(keyCode);
    
    if(window['msg'])   
        msg('(Settings.js?) keypress: code = ' + keyCode + ' - ' + akey  );
    
    // remember to let it fall back to original handler if we ignore it
    if(!this._navigate(akey)) {
        if(this.original_keypressed)
            return this.original_keypressed(keyCode);
    }
    
    return true;
};

// Focus control
Settings.prototype._changeDataFocus = function(key) {

    var curr = $('div#settings div#options a.active');
    var next = curr;
msg(curr);  
    if(curr.length <= 0) {
        this.idx = -1;
        return;
    }
        
    switch(key) {
        case 'UP':
        case 'LEFT':
            next = curr.prevAll('a').first();
            if(next.length<=0)
                next = $('div#settings div#options a:last-child');
            break;
        case 'DOWN':
        case 'RIGHT':
            next = curr.nextAll('a').first();
            if(next.length<=0)
                next = $('div#settings div#options a:first-child');
            break;
    }
    
    this._blur(curr);
    this._focus(next);
};

