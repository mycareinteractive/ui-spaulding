// ------------------------------------------------------------------------------
// public functions
// ------------------------------------------------------------------------------

// This is a hack for the old primary-movies panel design.  Since we don't have MVC in the design here we simulate a very basic MVC model.
// MODEL 		- The data passed in is a JSON object that contains all data this page needs.
// VIEW 		- We Ajax load movies.html and movies.css into DOM.
// CONTROLLER	- We then hijack keystroks so we can handle it first.  For those keys we ignore we let them fall back to original handler.
// 
// CSS engineer can take these 3 files (html+js+css) and start tweaking stylesheets without knowing anything about js logic.
// JS engineer can test this page alone without backend and any other javascript logic.  One page at a time, no worries about messing other pages.
function movies(wrapper, classStr, oncreate, ondestroy, horizontalMenu, backAtBottom, noAjax) {
    this.wrapper = wrapper;
    this.classStr = classStr;
    this.oncreate = oncreate;
    this.ondestroy = ondestroy;
    this.horizontalMenu = horizontalMenu;
    this.noAjax = noAjax;
    this.backAtBottom = backAtBottom;
    this.data = null;
    this.subdata = null;
    
    if(this.backAtBottom)
    	this.idx = 0;
    else
    	this.idx = 1;
}

movies.prototype.render = function(data,movie) {
	this.data = data;this.movie = movie;
	if(!this.noAjax) { // Ajax loading
    	// load movies.css
		$("<link/>", {rel: "stylesheet", type: "text/css", href: "css/movies.css"}).appendTo("head");
		
		// load movies.html
		var container = $('#' + this.wrapper);
		if(container.length<=0) {
			$('body').prepend('<div id="' + this.wrapper + '"></div>');
		}
		
		var context = this;
		
		$('#' + this.wrapper).load("movies.html #movies", function(){            // callback to outside so they can do stuff (such as hiding div)            if(context.oncreate)                context.oncreate();            applySettings('#movies');        
			context._renderData();
			context._initiatedata();
		});
    }
    else {    	this._renderData();
		this._initiatedata();
    }
}

movies.prototype.destroy = function() {
	// restore key handler
	this._restoreKeypressed();
	
	//remove HTML from DOM and remove CSS file link	
	$('#' + this.wrapper).remove();
	$('link[href="css/movies.css"]').remove();
	
	// callback to outside so they can bring up other UI
	if(this.ondestroy)
		this.ondestroy();
};

// ------------------------------------------------------------------------------
// internal functions
// ------------------------------------------------------------------------------
// All the keyboard event that we will process
movies.prototype._navigate = function(key)	{

	var curr = $("#movies #selections a.active");
		var id = $("#movies div#selection div#body div#carousel div#carouselPosters img.current").attr("id");							msg('id ' + id);		if (curr.length>=0 && (id<=0 || typeof id =='undefined') && key=="RIGHT") {			this._click(curr,key);		return true;	} else 	
	if (curr.length>=0 && key=='ENTER') {	
		this._click(curr,key);
		return true;
	}
	if ( key=='LEFT' && (id<=0 || typeof id == 'undefined')) {		this.destroy();		gotomainmenu();	}	
	if ( key=='MENU' || key =='HOME') {		this.destroy();		return false;	}	if ( key=='POWR' ||	key=='END')	{		this.destroy();		return false;	}
	
	var menuprev = "UP", menunext = "DOWN";
	var submenuprev = "LEFT", submenunext = "RIGHT";
	if(this.horizontalMenu) {	// horizontal menu uses left/right to change focus and up/down to further control sub menu
		menuprev = "LEFT";                              
		menunext = "RIGHT";
		submenuprev = "UP";
		submenunext = "DOWN";
	}
	if(key==menuprev || key==menunext) {
			this._changeDataFocus(key);	
		return true;
	}
	else if(key==submenuprev || key==submenunext) {
		this._changeSubDataFocus(key);	
		return true;
	}
	
	return false;
};

// When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
movies.prototype._click = function(jqobj,key) {	var id = $("#movies div#selection div#body div#carousel div#carouselPosters img.current").attr("id");							var selection = $("#movies div#selection div#body div#carousel div#carouselPosters img.current").attr("alt");							msg('id' + id + ' selection ' + selection);	msg('_click');	var backidx = this.backAtBottom? this.subdata.length : 0;		if(this.idx == backidx || key=='LEFT') { // back button		this.destroy();	} else {		var dataidx = this.backAtBottom? this.idx : this.idx-1;	msg('id ' + id);	if(typeof id =='undefined') 		this.initiateCarousel();	if(typeof this.subdata[dataidx] != 'undefined') {	var sub = this.subdata[dataidx];	} else {	var sub = this.subdata;	}		}	if(id>=1) {				if (!selection||selection==''||selection==' '||selection=='EMPTY3')	{				return;		}			$("#K_selection").removeAttr("class");		$("#K_selection").text('');		// selection statement was here //		$("#K_selection").addClass(selection);		var newpage = new watchmovie('watchmovie-wrapper', this.idx							, function(){								$('#movies-wrapper').hide();							}, function(){								$('#movies-wrapper').show();							},0,1);							var curr = $('div#movies div#selections a.active').attr("pouid");							var movie = $('#carouselPosters img.current').attr("alt");							msg('curr' + curr );							 $("#K_choice").removeClass("class");							 $("#K_choice").addClass(curr);							 var curr = $('div#movies div#selections a.active').attr("pouid");							var options = getSelections(curr);														//msg('movie ' + movie + ' selection ' + selection);									newpage.render(options,selection);	}		return;};

// When an object is focused by using keyboard, or mouse hover
movies.prototype._focus = function(jqobj) {
	jqobj.addClass('active');
	this.idx = jqobj.index();
	var backidx = this.backAtBottom? this.subdata.length : 0;
	if(this.idx < 0 || this.idx > this.subdata.length) {
		return;
	}
	else if(this.idx == backidx) { // back button
		$('#movies #background').removeClass('textoverlay');
	}
	else {
		$('#movies #background').addClass('textoverlay');
		this._renderSubData(jqobj);
	}
}

// When an object lose focus or mouse leave
movies.prototype._blur = function(jqobj) {
	if(jqobj) {
		jqobj.removeClass('active');
	}
	else {
		$('div#movies div#selections a.active').removeClass('active');
	}
	this.idx = -1;
	$('#selection #body').html('');
}

movies.prototype._renderData = function() {
    $('#' + this.wrapper + " #movies").addClass(this.classStr);
    var data = this.data;
	// show the div
	$('#' + this.wrapper + " #movies").show();

	$("#movies div#banner").html('<p class="banner">Use Up/Down to Select a Category and Left/Right to scroll the Movies</p>');			    
	
	$('<p></p>').prependTo('div#movies div#label').html('Movies');

	$('div#movies img#labelicon').attr('src', './images/icon_home_tv.png');
	var firstobj = null;
	
	for(var key in data.movies) {
			this.subdata = data.movies;
			var sub = this.data.movies[key];			

			var selections = $('div#movies div#selections');
		
			var Title = data.movies[key].label;
			var tag = data.movies[key].tag;
			
			var o = $('<a href="#"></a>').appendTo(selections).html(Title).addClass('menu ' + key).attr('poUID', tag);						if(key==0) {				firstobj = o;				}			
			}
		
			if(this.backAtBottom) {
				$('<a class="back" href="#" title="back">&lt; BACK</a>').appendTo(selections);
			}
			else {
			 $('<a class="back" href="#" title="back">&lt; BACK</a>').prependTo(selections);
			}			
			this._focus(firstobj);
			
			// wire up the mouse event
			$('div#movies div#selections a').click(function(){
				context._blur();
				context._focus($(this));
				context._click($(this));
				return false;
			});

	// now we take over the key handler from navigation.js
	this._overwriteKeypressed();
};

movies.prototype._renderSubData = function(jqobj) {
	var dataidx = this.backAtBottom? this.idx : this.idx-1;
	//msg('dataidx ' + dataidx + ' ' + this.idx);
	if(typeof this.subdata[dataidx] != 'undefined') {
		var sub = this.subdata[dataidx];
	} else {
	var sub = this.subdata;
	}
	this._renderPage(sub);
	
}

movies.prototype._initiatedata = function(jqobj) {
	if(typeof this.subdata[0] != 'undefined') {
		var sub = this.subdata[0];
	} else {
	var sub = this.subdata;
	}
	this._renderPage(sub);
	
}

movies.prototype._renderPage = function(jqobj) {
	var sub = jqobj;
	var carouselHTML = getCarouselHTML(sub.tag);
	this.carouseltotal = carouselHTML.total;
	this.carouselpages = carouselHTML.pages;
	$("#movies #selection #body").html(carouselHTML.carousel);

}

movies.prototype._renderPageList = function(jqobj) {
	var sub = jqobj;
					
	if(sub && sub['attributes'] && sub.attributes.attribute) {
		var attrlist = sub.attributes.attribute;
		var len = attrlist.length;
		var isPage1 = true;
		var pagebody = $('#movies #selection #body').html('');
		var pages = $('<div id="pages"></div>').appendTo(pagebody);
		for(var i=0; i<len; i++) {
			var attr = attrlist[i];
			if(attr.class=="body") { // text in pagelist
			if(isPage1) {
				isPage1 = false;
				attrstr = ' class="submenu active"';
			}
			else {
				attrstr=' class="submenu"';	
			}			
						
			pages.append('<p id=p' + i + attrstr + '>' + attr.text + '</p>');
			}
			
		}
		
		// support mouse driven page up/down
		var context = this;
		$('<div id="nextpage"></div>').appendTo(pagebody).click(function(){
			context._changeSubDataFocus('RIGHT');
			return false;
		});
		$('<div id="prevpage"></div>').appendTo(pagebody).click(function(){
			context._changeSubDataFocus('LEFT');
			return false;
		});
	}
}

// Hijack the global keypressed() function
movies.prototype._overwriteKeypressed = function() {
	this.original_keypressed = window.keypressed;
	var context = this;
	window.keypressed = function(keyCode) {
		context._keypressed(keyCode);
	}
};

// Restore the key handler
movies.prototype._restoreKeypressed = function() {
	window.keypressed = this.original_keypressed;
};

movies.prototype._keypressed = function(keyCode) {
	var akey = getkeys(keyCode);
	
	if(window['msg'])	
		msg('(movies.js) keypress: code = ' + keyCode + ' - ' + akey  );
	
	// remember to let it fall back to original handler if we ignore it
	if(!this._navigate(akey)) {
		if(this.original_keypressed)
			return this.original_keypressed(keyCode);
	}
	
	return true;
};

// Focus control
movies.prototype._changeDataFocus = function(key) {

	var curr = $('div#movies div#selections a.active');
	var next = curr;
	//curr.css("color","#ffffff");
	var dataidx = this.backAtBottom? this.idx : this.idx-1;
	if(typeof this.subdata[dataidx] != 'undefined') {	var sub = this.subdata[dataidx];
	} else {
	var sub = this.subdata;
	}
	msg('datafocus' + sub['type'] + ' key ' + key);	
	if(curr.length <= 0) {
		this.idx = -1;
		return;
	}
		
	switch(key) {
		case 'UP':
			next = curr.prev();
			if(next.length<=0)
				next = $('div#movies div#selections a:last-child');
			break;
		case 'DOWN':
			next = curr.next();
			if(next.length<=0)
				next = $('div#movies div#selections a:first-child');
			break;
		case 'RIGHT':
		case 'LEFT':
	
	}
	
	this._blur(curr);
	this._focus(next);
	var backidx = this.backAtBottom? this.subdata.length : 0;	
	if(this.idx == backidx) {
		this._initiatedata();	
	}
};

// Focus control
movies.prototype._changeSubDataFocus = function(key) {
	

	var id = $("#movies div#selection div#body div#carousel div#carouselPosters img.current").attr("id");						
	msg(id + ' ' + key);
	if(id>=1) {
		if(key=='RIGHT'||key=='LEFT') {	
			this.shiftCarousel(key);
			return true;
		} else
		if(key=='ENTER') {
				var newpage = new recovery('watchmovie-wrapper', currmenu
								, function(){
								$('#movies').hide();
								}, function(){
								$('#movies').show();
							});
						$("#K_movie").removeAttr("class");
						$("#K_movie").text('');
						var options = getMovieHTML();						
							newpage.render(options);
							return;
	
		}
	} 
	var curr = $('div#movies div#selection #body .submenu.active');
	var next = curr;

	var dataidx = this.backAtBottom? this.idx : this.idx-1;
	if(typeof this.subdata[dataidx] != 'undefined') {
	var sub = this.subdata[dataidx];
	} else {
	var sub = this.subdata;
	}

	if(curr.length <= 0) {
		return;
	}
		
	switch(key) {
		case 'UP':
		case 'LEFT':
			next = curr.prev();
			if(next.length<=0)
				next = $('div#movies div#selection #body .submenu:last-child');
			break;
		case 'DOWN':
		case 'RIGHT':
			next = curr.next();
			if(next.length<=0)
				next = $('div#movies div#selection #body .submenu:first-child');
			break;
		case 'ENTER':
			this.initiateCarousel();
			return true;
		}
	
	if(next != curr) {
		curr.removeClass('active');
		next.addClass('active');
	}
}



movies.prototype.initiateCarousel = function() {

	var curr = $("#movies #selections a.active");
	
	//curr.css("color","#A7D369");
	$("#movies div#selection div#body div#carousel div#carouselTitle").show();
	var title = $("#movies div#selection div#body div#carousel div#carouselPosters img#1").attr("title");
	$("#carouselTitle").html('<p style="display:block">'+title+'</p>');
	$("#movies div#selection div#body div#carousel div#carouselPosters img#1").removeClass("currentx");
	$("#movies div#selection div#body div#carousel div#carouselPosters img#1").addClass("current");
	var id = $("#movies div#selection div#body div#carousel div#carouselPosters img.current").attr("id");						
	this.carouselcurrentpage = '1';										
	
}

movies.prototype.shiftCarousel = function(direction)	{
	
	var ids = Array();
	
	var id   = $("#carouselPosters img.current").attr("id");
	var total = this.carouseltotal;
	var count = this.carouselpages;	msg('total ' + total + ' count ' + count);

	var ondeck = id;msg('ondeck before ' + ondeck);
	if (direction=='LEFT')	{
		ondeck--;
		if (ondeck == 0)
			ondeck = total;
	}	else
		if (direction=='RIGHT')	{
			ondeck++;
			if (ondeck > total)
				ondeck = 1;
		}	msg('ondeck after ' + ondeck);
	msg(' shift carousel id ' + id + ' ondeck ' + ondeck + ' count ' + count );	msg('ondec%3 '+ ondeck%3);
	if(ondeck%3 == 1 && (direction=='RIGHT')) {
		this.nextmoviepanel(direction);
	} else  
			if((ondeck%3 == 0) && (direction=='LEFT')) {
				this.nextmoviepanel(direction);
			} else						if((ondeck==total) && (direction=='LEFT')) {				this.nextmoviepanel(direction);			} 			
	
	$("#carouselPosters img#"+id).removeClass("current");
	$("#carouselPosters img#"+ondeck).removeClass("currentx");
	$("#carouselPosters img#"+id).addClass("currentx");
	$("#carouselPosters img#"+ondeck).addClass("current");
	
	var title = $("#carouselPosters img#"+ondeck).attr("title");
	var sub = $("#carouselPosters img#"+ondeck);
	msg(sub);
	
	$("#movies div#selection div#body div#carousel div#carouselTitle").html('<p style="display:block">'+title+'</p>');
	return;
}
movies.prototype.nextmoviepanel = function(key) {

	var id   = $("#carouselPosters img.current").attr("id");
	var total = this.carouseltotal;
	var count = this.carouselpages;
msg('count' + count);
	if (count==0)
		return;
	
	var id = $("#movies div#selection div#body div#carousel div#carouselPosters img.current").attr("id");						
	
	var active = this.carouselcurrentpage;
	var ondeck = active;msg('ondeck before ' + ondeck);
	if (key=='LEFT')	{
		ondeck--;
		if (ondeck == 0)
			ondeck = count;
	}	else
		if (key=='RIGHT')	{
			ondeck++;
			if (ondeck > count)
				ondeck = 1;
		}	msg('ondeck after ' + ondeck);
	msg(' nextmovie panel id ' + id + ' active ' + active + ' ondeck ' + ondeck + ' count ' + count );
	
	$("#movies div#selection div#body div#carousel div#carouselPosters.page"+active).css("display","none");
	$("#movies div#selection div#body div#carousel div#carouselPosters.page"+ondeck).css("display","block");
	this.carouselcurrentpage = ondeck;
	return;
}