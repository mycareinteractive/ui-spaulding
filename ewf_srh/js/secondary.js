// ------------------------------------------------------------------------------
// public functions
// ------------------------------------------------------------------------------

// This is a hack for the old primary-secondary panel design.  Since we don't have MVC in the design here we simulate a very basic MVC model.
// MODEL 		- The data passed in is a JSON object that contains all data this page needs.
// VIEW 		- We Ajax load secondary.html and secondary.css into DOM.
// CONTROLLER	- We then hijack keystroks so we can handle it first.  For those keys we ignore we let them fall back to original handler.
// 
// CSS engineer can take these 3 files (html+js+css) and start tweaking stylesheets without knowing anything about js logic.
// JS engineer can test this page alone without backend and any other javascript logic.  One page at a time, no worries about messing other pages.
function secondary (wrapper, classStr, oncreate, ondestroy, horizontalMenu, backAtBottom, noAjax) {
	msg(classStr); 
    this.wrapper = wrapper;
    this.classStr = classStr;
    this.oncreate = oncreate;
    this.ondestroy = ondestroy;
    this.horizontalMenu = horizontalMenu;
    this.noAjax = noAjax;
    this.backAtBottom = backAtBottom;
    this.data = null;
    this.subdata = null;
    
    if(this.backAtBottom)
    	this.idx = 0;
    else
    	this.idx = 1;
}

secondary.prototype.render = function(data) {
	this.data = data;
	msg(this.data);
	if(!this.noAjax) { // Ajax loading
    	// load secondary.css
		$("<link/>", {rel: "stylesheet", type: "text/css", href: "css/secondary.css"}).appendTo("head");
		
		// load secondary.html
		var container = $('#' + this.wrapper);
		if(container.length<=0) {
			$('body').prepend('<div id="' + this.wrapper + '"></div>');
		}
		
		var context = this;
		
		$('#' + this.wrapper).load("secondary.html #secondary", function(){            // callback to outside so they can do stuff (such as hiding div)            if(context.oncreate)                context.oncreate();            applySettings("#secondary");    
			context._renderData();
		});
    }
    else {
    	this._renderData();
    }

	
}

secondary.prototype.destroy = function() {
	// restore key handler
	this._restoreKeypressed();
	
	//remove HTML from DOM and remove CSS file link	
	$('#' + this.wrapper).remove();
	$('link[href="css/secondary.css"]').remove();
	
	// callback to outside so they can bring up other UI
	if(this.ondestroy)
		this.ondestroy();
};

// ------------------------------------------------------------------------------
// internal functions
// ------------------------------------------------------------------------------
// All the keyboard event that we will process
secondary.prototype._navigate = function(key)	{
	var panel = $("#K_panel").attr('class');						
	var curr = $("#secondary #selections a.active");msg(panel);
	if((key=='LEFT') && panel=='menutv') {		var stoptv = stopTV();		$('#secondary-wrapper').show();				$("#K_panel").removeAttr('class');		$("#K_panel").addClass('primary');				return true;	} 	
	if (curr.length>=0 && (key=='ENTER' || key=="LEFT")) {	
		this._click(curr,key);
		return true;
	}
	
	if ( key=='MENU' || key =='HOME') {		this.destroy();		gotomainmenu();		return false;	}	if ( key=='POWR' ||	key=='END')	{		this.destroy();		return false;	}
	
	var menuprev = "UP", menunext = "DOWN";
	var submenuprev = "LEFT", submenunext = "RIGHT";
	if(this.horizontalMenu) {	// horizontal menu uses left/right to change focus and up/down to further control sub menu
		menuprev = "LEFT";                              
		menunext = "RIGHT";
		submenuprev = "UP";
		submenunext = "DOWN";
	}
	
	if(key==menuprev || key==menunext) {
		this._changeDataFocus(key);	
		return true;
	}
	else if(key==submenuprev || key==submenunext) {
		this._changeSubDataFocus(key);	
		return true;
	}
	
	return false;
};

// When "ENTER" is pressed on a link, or when link is clicked by mouse or touch screen
secondary.prototype._click = function(jqobj,key) {
msg(this.wrapper);	
	var backidx = this.backAtBottom? this.subdata.length : 0;
	var panel = $("#K_panel").attr('class');						
	if(this.idx == backidx || key=='LEFT') { // back button
		this.destroy();
	} else {
		var dataidx = this.backAtBottom? this.idx : this.idx-1;
	
    	if(typeof this.subdata[dataidx] != 'undefined') {
    	var sub = this.subdata[dataidx];
    	} else {
    	var sub = this.subdata;
    	}
    	var type = sub['type'];
    	   if(sub['tag']=='weekataglance') {
    	       // this is patient meal page
    	        var ewf = ewfObject();
                var url  = ewf.host+ewf.proxy;
                var xdUrl= ewf.meals;
                var args = 'wsdl='+xdUrl;
                var mealData = ajaxJSON(url, args);
                
    	       //var mealData = ajaxJSON('getAgendaJson.json');
    	           	       var newpage = new Meal('meal-wrapper', this.classStr
                    , function(){
                        $('#secondary-wrapper').hide();
                    }, function(){
                        $('#secondary-wrapper').show();
                    }, true);
                    
                newpage.render(mealData);
    	   }		   		    else if(sub['tag']=='ccmenu') {				$("#K_panel").removeAttr('class').text('');				$("#K_panel").addClass('menutv');										// this is settings page											$('#primary').hide();								$('#secondary-wrapper').hide();										menuTV();										return true;				    	   }
    	   else if(sub['type']=='menu') {			
    			// this is a 3rd level menu.
    			// here we use the new MVC page - Tertiary object				var subclass = sub['tag'];
    			var newpage = new tertiary('tertiary-wrapper', subclass
    				, function(){
    					$('#secondary-wrapper').hide();
    				}, function(){
    					$('#secondary-wrapper').show();
    				});
    				
    			newpage.render(sub);
    	   }
				
	}
	
	return;
};

// When an object is focused by using keyboard, or mouse hover
secondary.prototype._focus = function(jqobj) {
	jqobj.addClass('active');
	this.idx = jqobj.index();
	var backidx = this.backAtBottom? this.subdata.length : 0;
	if(this.idx < 0 || this.idx > this.subdata.length) {
		return;
	}
	else if(this.idx == backidx) { // back button
		$('#secondary #background').removeClass('textoverlay');
	}
	else {
		$('#secondary #background').addClass('textoverlay');
		this._renderSubData(jqobj);
	}
}

// When an object lose focus or mouse leave
secondary.prototype._blur = function(jqobj) {
	if(jqobj) {
		jqobj.removeClass('active');
	}
	else {
		$('div#secondary div#selections a.active').removeClass('active');
	}
	this.idx = -1;
	$('#secondary #selection #body').html('');
}

secondary.prototype._renderData = function() {
    $('#' + this.wrapper + " #secondary").addClass(this.classStr);
    var data = this.data;
    msg(data);
	// show the div
	$('#' + this.wrapper + " #secondary").show();    applySettings("#secondary");msg('tag ' + data.tag);

	// dynamically render data if neccesary
	for(var key in data) {
		if (key=='tag') {	// tag
		}
		else if(key=='label') {	// label
			$('<p></p>').prependTo('div#secondary div#label').html(data[key]);
		}
		
		else if(key=='attributes') { // attributes (title and label icon)
			var attrlist = data.attributes.attribute;
			var len = attrlist.length;
			for(var i=0; i<len; i++) {
			var attr = attrlist[i];
			if (attr.class == 'banner1') {
				$("#secondary div#banner").html('<p class="banner">' + attr.text + '</p>');			    
			} 						else if (attr.class == 'instructions') {				$("#secondary div#instructions").html('<p class="i">' + attr.text + '</p>');			    			} 			
			else if (attr.class == 'banner2') {
			    	$('<span></span>').appendTo('div#secondary div#banner p').html('<br/>' + attr.text);			    	
			}
			else if (attr.class == 'icon') {
				$('div#secondary img#labelicon').attr('src', './images/'+attr.image);
			}			

			}
		}
		else if(key==data.tag) { // sub menu or children
			
			this.subdata = data[key];

			var sub = this.data[key];			
			var context = this;
			var firstobj = null;
			var selections = $('div#secondary div#selections');
			
			if(typeof data[key].label !='undefined') {
				var o = $('<a href="#"></a>').appendTo(selections).html(data[key].label).addClass('menu ' + data[key].tag);			
				firstobj = o;
			} else {

			$.each(data[key], function(i, v) {
				msg(v + i + data[key]);
				var o = $('<a href="#"></a>').appendTo(selections).html(v.label).addClass('menu ' + v.tag);
				if(i==0)
					firstobj = o;
			});

			}
			if(this.backAtBottom) {
				$('<a class="back" href="#" title="back">&lt; BACK</a>').appendTo(selections);
			}
			else {
				var firstobj = $('<a class="back" href="#" title="back">&lt; BACK</a>').prependTo(selections);
				
			}
			this._focus(firstobj);
			
			// wire up the mouse event
			$('div#secondary div#selections a').click(function(){
				context._blur();
				context._focus($(this));
				context._click($(this));
				return false;
			});
		}
	}
	
	// now we take over the key handler from navigation.js
	this._overwriteKeypressed();
};

secondary.prototype._renderSubData = function(jqobj) {
	var dataidx = this.backAtBottom? this.idx : this.idx-1;
	if(typeof this.subdata[dataidx] != 'undefined') {
	var sub = this.subdata[dataidx];
	} else {
	var sub = this.subdata;
	}
	if(sub['type']=='page' || sub['type']=='menu') {
		this._renderPage(sub);
	}
	else if(sub['type']=='pagelist') {
		this._renderPageList(sub);
	}
}

secondary.prototype._renderPage = function(jqobj) {
	var sub = jqobj;msg(sub);
	if(sub && sub['attributes'] && sub.attributes.attribute) {
		var attrlist = sub.attributes.attribute;
		var len = attrlist.length;
		
		if(typeof attrlist.class !='undefined') {
			if(attrlist.class=="body") { // text in page
				$("#secondary #selection #body").html('<p class="submenu active">' + attrlist.text + '</p>');
			} else if(attrlist.class=="footer") { // text in page					
					$('<p class="submenu active topmargin"></span>').appendTo('#secondary #selection #body').html(attrlist.text);			    					
			} else if (attrlist.class == 'banner1') {
				$("#secondary div#banner").html('<p class="banner">' + attrlist.text + '</p>');			    			
			} else if (attrlist.class == 'banner2') {
			    	$('<span></span>').appendTo('div#secondary div#banner p').html('<br/>' + attrlist.text);			    				} else if (attrlist.class == 'instructions') {				$("#secondary div#instructions").html('<p class="instructions">' + attrlist.text + '</p>');			    
			}
		
	
		} else
		for(var i=0; i<len; i++) {
		
			var attr = attrlist[i];
			
			if(attr.class=="body") { // text in page
				$("#secondary #selection #body").html('<p class="submenu active">' + attr.text + '</p>');
			} else if(attr.class=="footer") { // text in page					
				$('<p class="submenu active highlight topmargin"></span>').appendTo('#secondary #selection #body').html(attr.text);			    					
			} 	if (attr.class == 'banner1') {
				$("#secondary div#banner").html('<p class="banner">' + attr.text + '</p>');			    
			} else if (attr.class == 'banner2') {
			    $('<span></span>').appendTo('div#secondary div#banner p').html('<br/>' + attr.text);			    				} else if (attrlist.class == 'instructions') {				$("#secondary div#instructions").html('<p class="instructions">' + attrlist.text + '</p>');			    
			}

		}
	}
}

secondary.prototype._renderPageList = function(jqobj) {
		var sub = jqobj;	if(sub && sub['attributes'] && sub.attributes.attribute) {		var attrlist = sub.attributes.attribute;		var len = attrlist.length;		var isPage1 = true;		var pagebody = $('#secondary #selection #body').html('');		var pages = $('<div id="pages"></div>').appendTo(pagebody);		var page = 0;		for(var i=0; i<len; i++) {			var attr = attrlist[i];			if(attr.class=="body") { // text in pagelist			if(isPage1) {				isPage1 = false;				attrstr = ' class="submenu active"';			}			else {				attrstr=' class="submenu"';				}			page ++;						msg('sub.tag ' + sub.tag);			if(this.data.tag=='directory') {				$("#secondary div#banner").html('<p class="banner">Use Right / Left to turn the pages of the Hospital Directory.</p>');			    				$('<span></span>').appendTo('div#secondary div#banner p').html('<br/>');			    									$("#secondary div#instructions").html('<p class="instructions">Spaulding has been designed to offer a patient-focused environment of healing. We invite you to explore our hospital. </p>');			    				pages.append('<p id=p' + i + attrstr + '>' + '<span class="rjust" >&lt; Page ' + page + ' &gt; </span><br/> '+attr.text +'<span class=bottom>Floor 1 includes Outdoor Plaza</span>' +'</p>');				} else			if(sub.tag=='medicalstaff') {				$("#secondary div#banner").html('<p class="banner"> Use &lt; Left / Right &gt; Arrow keys to scroll pages </p>');			    				$('<span></span>').appendTo('div#secondary div#banner p').html('<br/>');			    									pages.append('<p id=p' + i + attrstr + '>' + '<span class="rjust" >&lt; Page ' + page + ' &gt; </span><br/> '+attr.text +'</p>');				}			}					}				// support mouse driven page up/down		var context = this;		$('<div id="nextpage"></div>').appendTo(pagebody).click(function(){			context._changeSubDataFocus('RIGHT');			return false;		});		$('<div id="prevpage"></div>').appendTo(pagebody).click(function(){			context._changeSubDataFocus('LEFT');			return false;		});	}	return;}

// Hijack the global keypressed() function
secondary.prototype._overwriteKeypressed = function() {
	this.original_keypressed = window.keypressed;
	var context = this;
	window.keypressed = function(keyCode) {
		context._keypressed(keyCode);
	}
};

// Restore the key handler
secondary.prototype._restoreKeypressed = function() {
	window.keypressed = this.original_keypressed;
};

secondary.prototype._keypressed = function(keyCode) {
	var akey = getkeys(keyCode);
	msg(akey);
	if(window['msg'])	
		msg('(secondary.js) keypress: code = ' + keyCode + ' - ' + akey  );
	
	// remember to let it fall back to original handler if we ignore it
	if(!this._navigate(akey)) {
		if(this.original_keypressed)
			return this.original_keypressed(keyCode);
	}
	
	return true;
};

// Focus control
secondary.prototype._changeDataFocus = function(key) {

	var curr = $('div#secondary div#selections a.active');
	var next = curr;

	var dataidx = this.backAtBottom? this.idx : this.idx-1;
	if(typeof this.subdata[dataidx] != 'undefined') {
	var sub = this.subdata[dataidx];
	} else {
	var sub = this.subdata;
	}
	msg('datafocus' + sub['type'] + ' key ' + key);	
	if(curr.length <= 0) {
		this.idx = -1;
		return;
	}
		
	switch(key) {
		case 'UP':
		case 'LEFT':
			next = curr.prev();
			if(next.length<=0)
				next = $('div#secondary div#selections a:last-child');			if (next.attr("class")=='menu constitutioncafe') 				next = next.prev();			
			break;
		case 'DOWN':
		case 'RIGHT':
			next = curr.next();
			if(next.length<=0)
				next = $('div#secondary div#selections a:first-child');			if (next.attr("class")=='menu constitutioncafe') 				next = next.next();							
			break;
	
	}
	this._blur(curr);	this._focus(next);
};

// Focus control
secondary.prototype._changeSubDataFocus = function(key) {
	
	var curr = $('div#secondary div#selection #body .submenu.active');
	var next = curr;

	var dataidx = this.backAtBottom? this.idx : this.idx-1;
	if(typeof this.subdata[dataidx] != 'undefined') {
	var sub = this.subdata[dataidx];
	} else {
	var sub = this.subdata;
	}

	if(curr.length <= 0) {
		return;
	}
		
	switch(key) {
		case 'UP':
		case 'LEFT':
			next = curr.prev();
			if(next.length<=0)
				next = $('div#secondary div#selection #body .submenu:last-child');
			break;
		case 'DOWN':
		case 'RIGHT':
			next = curr.next();
			if(next.length<=0)
				next = $('div#secondary div#selection #body .submenu:first-child');
			break;
	}
	
	if(next != curr) {
		curr.removeClass('active');
		next.addClass('active');
	}
}

